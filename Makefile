.NOTPARALLEL:

workdir = $(shell pwd)

docker-build:
	docker build -t ansible-gitlab:latest .

docker-run:
	docker run --rm -ti --mount src="${workdir}",target=/gitlab-ansible,type=bind ansible-gitlab:latest
